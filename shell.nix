{ pkgs ? (import ./nix/nixpkgs {})
}:

pkgs.mkShell {
  inputsFrom = [(import ./default.nix { inherit pkgs; })];
  buildInputs = with pkgs.ocamlPackages; [
    ocaml-lsp
    odoc
    utop
  ];
  shellHook = ''
    eval $(opam env)
  '';
}
