{ pkgs ? (import ../nixpkgs {})
}:

pkgs.ocamlPackages.buildDunePackage {
  pname = "json-data-encoding";
  version = "0.9.1";
  useDune2 = true;
  src = pkgs.fetchFromGitLab {
    owner = "nomadic-labs";
    repo = "json-data-encoding";
    rev = "4dbef7f83d184aa95ee04e9a044f6a0b06445796";
    sha256 = "sha256-jClgslienUVo3O6SDg19DL4ki14wAfx5IL3jB4r+NMs=";
  };
  buildInputs = with pkgs.ocamlPackages; [
  ];
  propagatedBuildInputs = with pkgs.ocamlPackages; [
    uri
  ];
  doCheck = false;
}
