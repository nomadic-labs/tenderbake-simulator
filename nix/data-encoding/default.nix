{ pkgs ? (import ../nixpkgs {})
}:

pkgs.ocamlPackages.buildDunePackage {
  pname = "data-encoding";
  version = "0.3";
  useDune2 = true;
  src = pkgs.fetchFromGitLab {
    owner = "nomadic-labs";
    repo = "data-encoding";
    rev = "ec00948c4ad431a101e66a5a8c855f9252f34959";
    sha256 = "sha256-b4VQ9t/H/Hs9EENq2o2CHGM9LdhyuPC8nXZEHPjUPOs=";
  };
  buildInputs = with pkgs.ocamlPackages; [
    ezjsonm
    zarith
  ];
  propagatedBuildInputs = with pkgs.ocamlPackages; [
    (import ../json-data-encoding { inherit pkgs; })
    (import ../json-data-encoding-bson { inherit pkgs; })
  ];
  doCheck = false;
}
